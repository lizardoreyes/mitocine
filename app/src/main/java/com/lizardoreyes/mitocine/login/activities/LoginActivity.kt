package com.lizardoreyes.mitocine.login.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.lizardoreyes.mitocine.home.activities.HomeActivity
import com.lizardoreyes.mitocine.databinding.ActivityLoginBinding
import com.lizardoreyes.mitocine.login.presenter.LoginPresenter
import com.lizardoreyes.mitocine.login.views.LoginView
import com.lizardoreyes.mitocine.register.activities.RegisterActivity

class LoginActivity : AppCompatActivity(), LoginView {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        presenter = LoginPresenter()
        presenter.init(this)

        binding.btnLogin.setOnClickListener {
            presenter.validateFields()
        }

        binding.tvRegister.setOnClickListener {
            presenter.goToSignUp()
        }

    }

    override fun goToHome() {
        // TODO: Add alert dialog

        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }

    override fun goToSignUp() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    override fun getUsername(): String {
        return binding.edtUsername.text.toString()
    }

    override fun getPassword(): String {
        return binding.edtPassword.text.toString()
    }

    override fun printMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showErrorUsernameMessage(message: String) {
        binding.tlUser.error = message
    }

    override fun hideUsernameErrorMessage() {
        binding.tlUser.error = null
    }

    override fun showErrorPasswordMessage(message: String) {
        binding.tlPassword.error = message
    }

    override fun hidePasswordErrorMessage() {
        binding.tlPassword.error = null
    }

    override fun showErrorMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun getContext(): Context = this
}