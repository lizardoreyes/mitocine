package com.lizardoreyes.mitocine.login.presenter

import android.os.Handler
import android.os.Looper
import com.lizardoreyes.mitocine.BasePresenter
import com.lizardoreyes.mitocine.R
import com.lizardoreyes.mitocine.database.AppDatabase
import com.lizardoreyes.mitocine.login.views.LoginView
import com.lizardoreyes.mitocine.utils.SharedPreferenceHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginPresenter: BasePresenter<LoginView> {
    private lateinit var view: LoginView

    override fun init(view: LoginView) {
        this.view = view
    }

    fun validateFields(){
        if (!isFieldsNotValid()) {

            GlobalScope.launch {
                val user = withContext(Dispatchers.IO) {
                    val database = AppDatabase.getInstance(view.getContext())
                    database.personDao().validateUser(view.getUsername(), view.getPassword())
                }

                if (user != null) {

                    // Guardamos el usuario en el shared preferences
                    val preferences = SharedPreferenceHelper()
                    preferences.saveUsername(view.getContext(), view.getUsername())

                    view.goToHome()
                } else {
                    Handler(Looper.getMainLooper()).post {
                        view.showErrorMessage("No existe el usuario")
                    }
                }
            }
        }
    }

    private fun isFieldsNotValid(): Boolean {
        var isNotInvalid = false
        val username = view.getUsername()
        val password = view.getPassword()

        // Username
        if (username.isBlank()) {
            view.showErrorUsernameMessage(view.getContext().getString(R.string.err_username))
            isNotInvalid = true
        } else {
            view.hideUsernameErrorMessage()
        }

        // Password
        if (password.isBlank()) {
            view.showErrorPasswordMessage(view.getContext().getString(R.string.err_password))
            isNotInvalid = true
        } else {
            view.hidePasswordErrorMessage()
        }

        return isNotInvalid
    }

    fun goToSignUp() {
        view.goToSignUp()
    }
}