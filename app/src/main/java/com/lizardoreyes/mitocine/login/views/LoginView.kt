package com.lizardoreyes.mitocine.login.views

import android.content.Context
import com.lizardoreyes.mitocine.BaseView

interface LoginView: BaseView {
    fun goToHome()
    fun goToSignUp()
    fun getUsername(): String
    fun getPassword(): String

    fun printMessage(message: String)

    fun showErrorUsernameMessage(message: String)
    fun hideUsernameErrorMessage()
    fun showErrorPasswordMessage(message: String)
    fun hidePasswordErrorMessage()
    fun showErrorMessage(message: String)
}