package com.lizardoreyes.mitocine

import android.app.Application
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.google.android.libraries.places.api.Places

class AndroidApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        Glide.init(this, GlideBuilder())
        Places.initialize(this, getString(R.string.google_maps_key))
    }
}