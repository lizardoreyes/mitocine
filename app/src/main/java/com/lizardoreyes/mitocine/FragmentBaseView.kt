package com.lizardoreyes.mitocine

import android.content.Context

interface FragmentBaseView {
    fun getFragmentContext(): Context
}