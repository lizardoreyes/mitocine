package com.lizardoreyes.mitocine.home.fragments.menu

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.lizardoreyes.mitocine.R
import com.lizardoreyes.mitocine.databinding.FragmentSettingBinding
import com.lizardoreyes.mitocine.home.presenter.SettingPresenter
import com.lizardoreyes.mitocine.home.views.SettingView
import com.lizardoreyes.mitocine.login.activities.LoginActivity
import com.lizardoreyes.mitocine.map.activity.MapsActivity

class SettingFragment : Fragment(R.layout.fragment_setting), SettingView {
    private lateinit var binding: FragmentSettingBinding
    lateinit var presenter: SettingPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentSettingBinding.bind(view)

        presenter = SettingPresenter()
        presenter.init(this)

        initUI()

    }

    private fun initUI() {
        Glide.with(this)
            .load(R.drawable.ic_launcher_background)
            .circleCrop()
            .into(binding.ivAvatar)

        presenter.getUserDetail()

        binding.ivOpenMap.setOnClickListener {
            goToMap()
        }
        binding.ivOpenSearchMap.setOnClickListener {
            goToSearchMap()
        }

        binding.btnLogout.setOnClickListener {
            presenter.logout()
        }
    }

    private fun validateLogout() {
        val dialog = AlertDialog.Builder(requireContext())
        dialog.setTitle("Confirmación")
        dialog.setMessage("Esta seguro de salir de la sesión?")
        dialog.setPositiveButton("Salir") { _, _ -> presenter.logout() }
        dialog.setPositiveButton("No") { dialog, _ -> dialog.dismiss() }
    }

    private val AUTOCOMPLETE_REQUEST_CODE = 1
    private fun goToSearchMap() {
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        val fields = listOf(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME, Place.Field.ADDRESS)

        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
            .setCountry("pe")
            .build(requireContext())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        Log.i("TAG", "Place: ${place.name}, ${place.id} - ${place.latLng?.latitude} | ${place.latLng?.longitude}")

                        val intent = Intent(requireContext(), MapsActivity::class.java)
                        intent.putExtra("place", place)
                        startActivity(intent)
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.i("TAG", status.statusMessage ?: "No hay mensaje")
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun goToMap() {
        val intent = Intent(requireContext(), MapsActivity::class.java)
        startActivity(intent)
    }

    override fun showUsername(username: String) {
        binding.edtUsername.setText(username)
    }

    override fun showName(name: String) {
        binding.edtName.setText(name)
    }

    override fun showLastName(lastName: String) {
        binding.edtLastName.setText(lastName)
    }

    override fun showEmail(email: String) {
        binding.edtMail.setText(email)
    }

    override fun showAddress(address: String) {
        binding.tvAddress.text = address
    }

    override fun showPhone(phone: String) {
        binding.edtPhone.setText(phone)
    }

    override fun goToLogin() {
        val intent = Intent(requireContext(), LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun getFragmentContext(): Context = requireContext()
}