package com.lizardoreyes.mitocine.home.presenter

import com.lizardoreyes.mitocine.BasePresenter
import com.lizardoreyes.mitocine.home.views.HomeView

class HomePresenter: BasePresenter<HomeView> {
    lateinit var view: HomeView

    override fun init(view: HomeView) {
        this.view = view
    }
}