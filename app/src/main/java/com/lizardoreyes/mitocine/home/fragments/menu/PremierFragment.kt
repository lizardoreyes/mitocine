package com.lizardoreyes.mitocine.home.fragments.menu

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.lizardoreyes.mitocine.R
import com.lizardoreyes.mitocine.databinding.FragmentPremierBinding
import com.lizardoreyes.mitocine.home.adapter.PremierAdapter

class PremierFragment : Fragment(R.layout.fragment_premier) {
    private lateinit var binding: FragmentPremierBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentPremierBinding.bind(view)

        // Asignamos un adaptador
        binding.viewPager.adapter = PremierAdapter(parentFragmentManager, lifecycle)

        // Agregamos los Tabs al TabLayout
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = requireContext().getString(R.string.title_premier)
                    tab.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_movie)
                }
                1 -> {
                    tab.text = requireContext().getString(R.string.title_soon)
                    tab.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_soon)
                }
            }
        }.attach()
    }
}