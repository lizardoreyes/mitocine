package com.lizardoreyes.mitocine.home.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movie(val title: String, val imageUrl: String): Parcelable
