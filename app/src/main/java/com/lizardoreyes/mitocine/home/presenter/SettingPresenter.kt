package com.lizardoreyes.mitocine.home.presenter

import com.lizardoreyes.mitocine.BasePresenter
import com.lizardoreyes.mitocine.database.AppDatabase
import com.lizardoreyes.mitocine.home.views.SettingView
import com.lizardoreyes.mitocine.utils.SharedPreferenceHelper
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class SettingPresenter: BasePresenter<SettingView> {
    lateinit var view: SettingView

    override fun init(view: SettingView) {
        this.view = view
    }

    fun getUserDetail() {
        val preferences = SharedPreferenceHelper()
        val username = preferences.getUsername(view.getFragmentContext())

        doAsync {
            val database = AppDatabase.getInstance(view.getFragmentContext())
            val user = database.personDao().existsUser(username)
            uiThread {
                view.showUsername(user.username)
                view.showName(user.name)
                view.showLastName(user.lastName)
                view.showEmail(user.email)
                view.showAddress(user.address)
                view.showPhone(user.phone)
            }
        }
    }

    fun logout() {
        val preferences = SharedPreferenceHelper()
        preferences.clearAll(view.getFragmentContext())

        view.goToLogin()
    }
}