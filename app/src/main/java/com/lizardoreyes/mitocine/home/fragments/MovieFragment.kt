package com.lizardoreyes.mitocine.home.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.lizardoreyes.mitocine.R
import com.lizardoreyes.mitocine.databinding.FragmentMovieBinding
import com.lizardoreyes.mitocine.home.adapter.MovieListAdapter
import com.lizardoreyes.mitocine.home.adapter.MovieListCallback
import com.lizardoreyes.mitocine.home.model.Movie
import com.lizardoreyes.mitocine.movie_detail.activities.MovieDetailActivity

private const val ARG_TYPE = ""

class MovieFragment : Fragment(R.layout.fragment_movie), MovieListCallback {
    private lateinit var binding: FragmentMovieBinding
    private var paramType: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            paramType = it.getString(ARG_TYPE)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMovieBinding.bind(view)

        // TODO: Hasta el lunes en la mañana
        // Email: android@mitocodenetwork.com

        // https://api.themoviedb.org/3/movie/upcoming?api_key=83964036384cba93bbd32158c14ab9d7
        val BASE_URL = "https://image.tmdb.org/t/p/w500"

        when (paramType) {
            PREMIERS -> {
                val movies = arrayListOf(
                    Movie("Those Who Wish Me Dead", "$BASE_URL/xCEg6KowNISWvMh8GvPSxtdf9TO.jpg"),
                    Movie("The Unholy", "$BASE_URL/b4gYVcl8pParX8AjkN90iQrWrWO.jpg"),
                    Movie("Godzilla vs. Kong", "$BASE_URL/pgqgaUx1cJb5oZQQ5v0tNARCeBp.jpg"),
                    Movie("Nobody", "$BASE_URL/oBgWY00bEFeZ9N25wWVyuQddbAo.jpg"),
                    Movie("Cruella", "$BASE_URL/hjS9mH8KvRiGHgjk6VUZH7OT0Ng.jpg"),
                    Movie("Chaos Walking", "$BASE_URL/9kg73Mg8WJKlB9Y2SAJzeDKAnuB.jpg"),
                    Movie("Monster Hunter", "$BASE_URL/1UCOF11QCw8kcqvce8LKOO6pimh.jpg"),
                    Movie("Tom & Jerry", "$BASE_URL/8XZI9QZ7Pm3fVkigWJPbrXCMzjq.jpg"),
                    Movie("F9", "$BASE_URL/bOFaAXmWWXC3Rbv4u4uM9ZSzRXP.jpg"),
                    Movie("Soul", "$BASE_URL/hm58Jw4Lw8OIeECIq5qyPYhAeRJ.jpg")
                )
                val adapter = MovieListAdapter()
                adapter.setMovieCallback(this)
                binding.recyclerview.adapter = adapter
                binding.recyclerview.layoutManager = GridLayoutManager(requireContext(), 2)
                adapter.add(movies)
            }
            SOON -> {
                val movies2 = arrayListOf(
                    Movie("Skylines", "$BASE_URL/2W4ZvACURDyhiNnSIaFPHfNbny3.jpg"),
                    Movie("A Quiet Place Part II", "$BASE_URL/4q2hz2m8hubgvijz8Ez0T2Os2Yv.jpg"),
                    Movie("Wrath of Man", "$BASE_URL/YxopfHpsCV1oF8CZaL4M3Eodqa.jpg"),
                    Movie("Spiral: From the Book of Saw", "$BASE_URL/lcyKve7nXRFgRyms9M1bndNkKOx.jpg"),
                    Movie("Grand Isle", "$BASE_URL/8mcXb3km7hZ8aJKpxxgnvvxt9gW.jpg"),
                    Movie("Creation Stories", "$BASE_URL/7sEbuICGmFqooR5dbYQQpQa1zYo.jpg")
                )
                val adapter = MovieListAdapter()
                adapter.setMovieCallback(this)
                binding.recyclerview.adapter = adapter
                binding.recyclerview.layoutManager = GridLayoutManager(requireContext(), 2)
                adapter.add(movies2)
            }
            FAVORITE -> {
                val movies = arrayListOf(
                    Movie("Monster Hunter", "$BASE_URL/1UCOF11QCw8kcqvce8LKOO6pimh.jpg"),
                    Movie("Tom & Jerry", "$BASE_URL/8XZI9QZ7Pm3fVkigWJPbrXCMzjq.jpg"),
                    Movie("Godzilla vs. Kong", "$BASE_URL/pgqgaUx1cJb5oZQQ5v0tNARCeBp.jpg"),
                    Movie("Nobody", "$BASE_URL/oBgWY00bEFeZ9N25wWVyuQddbAo.jpg"),
                    Movie("Cruella", "$BASE_URL/hjS9mH8KvRiGHgjk6VUZH7OT0Ng.jpg"),
                    Movie("Those Who Wish Me Dead", "$BASE_URL/xCEg6KowNISWvMh8GvPSxtdf9TO.jpg")
                )
                val adapter = MovieListAdapter()
                adapter.setMovieCallback(this)
                binding.recyclerview.adapter = adapter
                binding.recyclerview.layoutManager = GridLayoutManager(requireContext(), 2)
                adapter.add(movies)
            }
        }
    }

    companion object {
        const val PREMIERS = "premiers"
        const val SOON = "soon"
        const val FAVORITE = "favorite"

        fun newInstance(type: String) =
            MovieFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_TYPE, type)
                }
            }
    }

    override fun onItemSelected(movie: Movie) {
        val intent = Intent(requireContext(), MovieDetailActivity::class.java)
        intent.putExtra("movie", movie)
        startActivity(intent)
    }

}