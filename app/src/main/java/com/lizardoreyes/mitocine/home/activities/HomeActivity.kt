package com.lizardoreyes.mitocine.home.activities

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lizardoreyes.mitocine.R
import com.lizardoreyes.mitocine.databinding.ActivityHomeBinding
import com.lizardoreyes.mitocine.home.fragments.menu.FavouriteFragment
import com.lizardoreyes.mitocine.home.fragments.menu.PremierFragment
import com.lizardoreyes.mitocine.home.fragments.menu.SettingFragment
import com.lizardoreyes.mitocine.home.presenter.HomePresenter
import com.lizardoreyes.mitocine.home.views.HomeView

class HomeActivity : AppCompatActivity(), HomeView {
    private lateinit var binding: ActivityHomeBinding
    private lateinit var presenter: HomePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        presenter = HomePresenter()
        presenter.init(this)

        this.title = getString(R.string.app_name)

        setDefaultFragment()

        binding.bnvMenu.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.premiers -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.flContainer, PremierFragment())
                        .commit()
                    true
                }
                R.id.favorites -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.flContainer, FavouriteFragment())
                        .commit()
                    true
                }
                R.id.settings -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.flContainer, SettingFragment())
                        .commit()
                    true
                }
                else -> {
                    false
                }
            }
        }
    }

    private fun setDefaultFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.flContainer, PremierFragment())
            .commit()
    }

    override fun getContext(): Context = this
}