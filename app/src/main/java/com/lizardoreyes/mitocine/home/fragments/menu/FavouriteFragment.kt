package com.lizardoreyes.mitocine.home.fragments.menu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.lizardoreyes.mitocine.R
import com.lizardoreyes.mitocine.databinding.FragmentFavouriteBinding
import com.lizardoreyes.mitocine.home.fragments.MovieFragment

class FavouriteFragment : Fragment(R.layout.fragment_favourite) {
    private lateinit var binding: FragmentFavouriteBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentFavouriteBinding.bind(view)

        /*
        val BASE_URL = "https://image.tmdb.org/t/p/w500"
        val movies = arrayListOf(
            Movie("Monster Hunter", "$BASE_URL/1UCOF11QCw8kcqvce8LKOO6pimh.jpg"),
            Movie("Tom & Jerry", "$BASE_URL/8XZI9QZ7Pm3fVkigWJPbrXCMzjq.jpg"),
            Movie("Godzilla vs. Kong", "$BASE_URL/pgqgaUx1cJb5oZQQ5v0tNARCeBp.jpg"),
            Movie("Nobody", "$BASE_URL/oBgWY00bEFeZ9N25wWVyuQddbAo.jpg"),
            Movie("Cruella", "$BASE_URL/hjS9mH8KvRiGHgjk6VUZH7OT0Ng.jpg"),
            Movie("Those Who Wish Me Dead", "$BASE_URL/xCEg6KowNISWvMh8GvPSxtdf9TO.jpg")
        )
        val adapter = MovieListAdapter()
        binding.recyclerview.adapter = adapter
        binding.recyclerview.layoutManager = GridLayoutManager(requireContext(), 2)
        adapter.add(movies)
        */

        parentFragmentManager.beginTransaction()
            .replace(R.id.flContainer, MovieFragment.newInstance(MovieFragment.FAVORITE))
            .commit()
    }
}