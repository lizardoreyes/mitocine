package com.lizardoreyes.mitocine.home.views

import com.lizardoreyes.mitocine.FragmentBaseView

interface SettingView: FragmentBaseView {
    fun showUsername(username: String)
    fun showName(name: String)
    fun showLastName(lastName: String)
    fun showEmail(email: String)
    fun showAddress(address: String)
    fun showPhone(phone: String)
    fun goToLogin()
}