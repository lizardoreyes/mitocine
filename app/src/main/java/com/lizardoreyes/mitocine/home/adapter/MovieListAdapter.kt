package com.lizardoreyes.mitocine.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lizardoreyes.mitocine.R
import com.lizardoreyes.mitocine.databinding.ItemMovieBinding
import com.lizardoreyes.mitocine.home.model.Movie
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso

class MovieListAdapter(
    private val movies: ArrayList<Movie> = arrayListOf()
): RecyclerView.Adapter<MovieListAdapter.MovieListViewHolder>() {
    private lateinit var binding: ItemMovieBinding
    lateinit var callback: MovieListCallback

    inner class MovieListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val titleMovie: TextView = binding.tvTitleMovie
        private val imgMovie: ImageView = binding.imgMovie

        fun bind(movie: Movie) {
            titleMovie.text = movie.title
            //Picasso.get().load(movie.imageUrl).placeholder(R.drawable.no_movie).memoryPolicy(MemoryPolicy.NO_CACHE).fit().into(imgMovie)
            Glide.with(itemView).load(movie.imageUrl).placeholder(R.drawable.no_movie).into(imgMovie)

            binding.root.setOnClickListener {
                callback.onItemSelected(movie)
            }
        }
    }

    fun setMovieCallback(callback: MovieListCallback) {
        this.callback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListViewHolder {
        binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context))
        return MovieListViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: MovieListViewHolder, position: Int) {
        holder.bind(movies[position])
    }

    override fun getItemCount(): Int = this.movies.size

    fun add(movies: ArrayList<Movie>) {
        this.movies.addAll(movies)
        notifyDataSetChanged()
    }

}

interface MovieListCallback {
    fun onItemSelected(movie: Movie)
}