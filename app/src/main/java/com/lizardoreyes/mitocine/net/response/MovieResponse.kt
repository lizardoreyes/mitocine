package com.lizardoreyes.mitocine.net.response

class MovieResponse {
    val data: List<MovieDataResponse>? = null
    val status: Boolean? = null
    val message: String? = null
}

class MovieDataResponse {
    var id: Int = 0
    var title: String = ""
    var description: String = ""
    var urlImage: String = ""
    var releaseDate: String = ""
}