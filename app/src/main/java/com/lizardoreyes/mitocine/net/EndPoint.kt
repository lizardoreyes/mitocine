package com.lizardoreyes.mitocine.net

import com.lizardoreyes.mitocine.net.response.MovieResponse
import com.lizardoreyes.mitocine.net.response.ProductDataResponse
import com.lizardoreyes.mitocine.net.response.ProductResponse
import com.lizardoreyes.mitocine.net.response.UserDataResponse
import retrofit2.Call
import retrofit2.http.*

interface EndPoint {
    @GET("movie/premieres")
    fun getPremieres(): Call<MovieResponse>

    @GET("movie/other")
    fun getOther(): Call<MovieResponse>

    @GET("product")
    fun getProduct(): Call<ProductResponse>

    @GET("product/{id}")
    fun getProduct(@Path("id") id: Int): Call<ProductDataResponse>

    @POST("product")
    fun addProduct(@Body name: String, @Body description: String, @Body price: String,
                   @Body url_image: String): Call<ProductResponse>

    @PUT("product/{id}")
    fun updateProduct(@Path("id") id: Int, @Body name: String,
                      @Body description: String, @Body price: String, @Body url_image: String): Call<ProductResponse>

    @POST("user/validate")
    fun validateUser(@Body username: String, @Body password: String): Call<UserDataResponse>

    @POST("user")
    fun addUser(@Body name: String, @Body last_name: String, @Body username: String,
                @Body password: String, @Body email: String, @Body address: String, @Body phone: String): Call<UserDataResponse>
}