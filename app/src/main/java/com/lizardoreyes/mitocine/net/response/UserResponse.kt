package com.lizardoreyes.mitocine.net.response

class UserResponse {
    val data: List<UserDataResponse>? = null
    val status: Boolean? = null
    val message: String? = null
}

class UserDataResponse {
    var id: Int = 0
    var name: String = ""
    var last_name: String = ""
    var username: String = ""
    var password: String = ""
    var email: String = ""
    var address: String = ""
    var phone: String = ""
}