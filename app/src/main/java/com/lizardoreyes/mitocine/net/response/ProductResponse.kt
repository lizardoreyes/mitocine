package com.lizardoreyes.mitocine.net.response

class ProductResponse {
    val data: List<ProductDataResponse>? = null
    val status: Boolean? = null
    val message: String? = null
}

class ProductDataResponse {
    var id: Int = 0
    var name: String = ""
    var description: String = ""
    var price: Int = 0
    var urlImage: String = ""
    var status: Boolean = false
}