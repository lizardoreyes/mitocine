package com.lizardoreyes.mitocine.utils

import android.content.Context

class SharedPreferenceHelper {

    companion object {
       const val MITOCINE = "mitocine"
       const val KEY_USERNAME = "username"
   }

    fun saveUsername(context: Context, username: String) {
        val preferences = context.getSharedPreferences(MITOCINE, Context.MODE_PRIVATE).edit()
        preferences.putString(KEY_USERNAME, username)
        preferences.apply()
    }

    fun getUsername(context: Context): String {
        val preferences = context.getSharedPreferences(MITOCINE, Context.MODE_PRIVATE)
        return preferences.getString(KEY_USERNAME, "") ?: ""
    }

    fun clearAll(context: Context) {
        val preferences = context.getSharedPreferences(MITOCINE, Context.MODE_PRIVATE).edit()
        preferences.clear()
        preferences.apply()
    }
}