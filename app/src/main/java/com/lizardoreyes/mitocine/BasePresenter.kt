package com.lizardoreyes.mitocine

import android.content.Context

interface BasePresenter<T> {
    fun init(view: T)
}