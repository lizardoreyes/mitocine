package com.lizardoreyes.mitocine.splash.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.lizardoreyes.mitocine.databinding.ActivitySplashBinding
import com.lizardoreyes.mitocine.home.activities.HomeActivity
import com.lizardoreyes.mitocine.login.activities.LoginActivity
import com.lizardoreyes.mitocine.splash.presenter.SplashPresenter
import com.lizardoreyes.mitocine.splash.views.SplashView

class SplashActivity : AppCompatActivity(), SplashView {
    private lateinit var presenter: SplashPresenter
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        presenter = SplashPresenter()
        presenter.init(this)
        presenter.verifyRedirect()

    }

    override fun goToLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun getContext(): Context = this

    override fun goToHome() {
        val intent = Intent(this, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }
}