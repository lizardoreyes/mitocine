package com.lizardoreyes.mitocine.splash.views

import android.content.Context

interface SplashView {
    fun goToLogin()
    fun getContext(): Context
    fun goToHome()
}