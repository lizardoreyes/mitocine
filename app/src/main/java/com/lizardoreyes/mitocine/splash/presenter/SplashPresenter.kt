package com.lizardoreyes.mitocine.splash.presenter

import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.lizardoreyes.mitocine.BasePresenter
import com.lizardoreyes.mitocine.net.EndPoint
import com.lizardoreyes.mitocine.net.RetrofitConfiguration
import com.lizardoreyes.mitocine.net.response.MovieResponse
import com.lizardoreyes.mitocine.splash.views.SplashView
import com.lizardoreyes.mitocine.utils.SharedPreferenceHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.log

class SplashPresenter: BasePresenter<SplashView> {
    private lateinit var view: SplashView

    override fun init(view: SplashView) {
        this.view = view
    }

    fun verifyRedirect() {
        val retrofit = RetrofitConfiguration.getConfiguration().create(EndPoint::class.java)
        val call = retrofit.getPremieres()

        call.enqueue(object: Callback<MovieResponse> {
            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                if(response.isSuccessful) {
                    val movies = response.body()
                    movies?.let {
                        movies.data?.forEach {
                            Log.e("TAG", "onResponse: ${it.title}", )
                        }
                    }
                } else {
                    Toast.makeText(view.getContext(), "${response.code()}: ${response.message()}", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Toast.makeText(view.getContext(), t.message.toString(), Toast.LENGTH_SHORT).show()
            }
        })

        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            val preferences = SharedPreferenceHelper()
            if (preferences.getUsername(view.getContext()) != "") {
                view.goToHome()
            } else {
                view.goToLogin()
            }
        }, 2000)

    }
}