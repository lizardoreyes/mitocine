package com.lizardoreyes.mitocine.register.views

import com.lizardoreyes.mitocine.BaseView

interface RegisterView: BaseView {
    fun getUsername(): String
    fun getName(): String
    fun getLastName(): String
    fun getAddress(): String
    fun getPassword(): String
    fun getMail(): String
    fun getPhone(): String

    fun printMessage(message: String)

    fun signUpSuccess(title: String, message: String)
    fun goToHome()

    fun showErrorUsernameMessage(message: String)
    fun hideUsernameErrorMessage()
    fun showErrorPasswordMessage(message: String)
    fun hidePasswordErrorMessage()
    fun showErrorNameMessage(message: String)
    fun hideNameErrorMessage()
    fun showErrorLastNameMessage(message: String)
    fun hideLastNameErrorMessage()
    fun showErrorAddressMessage(message: String)
    fun hideAddressErrorMessage()
    fun showErrorMailMessage(message: String)
    fun hideMailErrorMessage()
    fun showErrorPhoneMessage(message: String)
    fun hidePhoneErrorMessage()
}