package com.lizardoreyes.mitocine.register.presenter

import com.lizardoreyes.mitocine.BasePresenter
import com.lizardoreyes.mitocine.R
import com.lizardoreyes.mitocine.database.AppDatabase
import com.lizardoreyes.mitocine.database.Person
import com.lizardoreyes.mitocine.register.views.RegisterView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RegisterPresenter: BasePresenter<RegisterView> {
    private lateinit var view: RegisterView
    private var person: Person = Person()

    override fun init(view: RegisterView) {
        this.view = view
    }

    fun validateFields() {

        if (!isFieldsNotValid()) {

            GlobalScope.launch {
                val database = AppDatabase.getInstance(view.getContext())
                database.personDao().insert(person)
            }

            view.signUpSuccess(
                view.getContext().getString(R.string.str_title_sign_up) ,
                view.getContext().getString(R.string.msg_sign_up_success)
            )
        }
    }

    private fun isFieldsNotValid(): Boolean {

        var isNotInvalid = false
        val username = view.getUsername()
        val name = view.getName()
        val lastName = view.getLastName()
        val address = view.getAddress()
        val mail = view.getMail()
        val phone = view.getPhone()
        val password = view.getPassword()

        // Username
        if (username.isBlank()) {
            view.showErrorUsernameMessage(view.getContext().getString(R.string.err_username))
            isNotInvalid = true
        } else {
            view.hideUsernameErrorMessage()
        }

        // Name
        if (name.isBlank()) {
            view.showErrorNameMessage(view.getContext().getString(R.string.err_name))
            isNotInvalid = true
        } else {
            view.hideNameErrorMessage()
        }

        // lastName
        if (lastName.isBlank()) {
            view.showErrorLastNameMessage(view.getContext().getString(R.string.err_lastName))
            isNotInvalid = true
        } else {
            view.hideLastNameErrorMessage()
        }

        // Address
        if (address.isBlank()) {
            view.showErrorAddressMessage(view.getContext().getString(R.string.err_address))
            isNotInvalid = true
        } else {
            view.hideAddressErrorMessage()
        }

        // Mail
        if (mail.isBlank()) {
            view.showErrorMailMessage(view.getContext().getString(R.string.err_mail))
            isNotInvalid = true
        } else {
            view.hideMailErrorMessage()
        }

        // Phone
        if (phone.isBlank()) {
            view.showErrorPhoneMessage(view.getContext().getString(R.string.err_phone))
            isNotInvalid = true
        } else {
            view.hidePhoneErrorMessage()
        }

        // Password
        if (password.isBlank()) {
            view.showErrorPasswordMessage(view.getContext().getString(R.string.err_password))
            isNotInvalid = true
        } else {
            view.hidePasswordErrorMessage()
        }

        person.name = name
        person.username = username
        person.address = address
        person.email = mail
        person.password = password
        person.phone = phone

        return isNotInvalid
    }

    fun goToHome() {
        view.goToHome()
    }
}