package com.lizardoreyes.mitocine.register.activities

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.lizardoreyes.mitocine.home.activities.HomeActivity
import com.lizardoreyes.mitocine.R
import com.lizardoreyes.mitocine.databinding.ActivityRegisterBinding
import com.lizardoreyes.mitocine.register.presenter.RegisterPresenter
import com.lizardoreyes.mitocine.register.views.RegisterView

class RegisterActivity : AppCompatActivity(), RegisterView {
    private lateinit var presenter: RegisterPresenter
    private lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        this.title = "Registrarse Ahora"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter = RegisterPresenter()
        presenter.init(this)
        
        binding.btnRegister.setOnClickListener { 
            presenter.validateFields()
        }
    }

    override fun getUsername(): String {
        return binding.edtUsername.text.toString()
    }

    override fun getName(): String {
        return binding.edtName.text.toString()
    }

    override fun getLastName(): String {
        return binding.edtLastName.text.toString()
    }

    override fun getAddress(): String {
        return binding.edtAddress.text.toString()
    }

    override fun getPassword(): String {
        return binding.edtPassword.text.toString()
    }

    override fun getMail(): String {
        return binding.edtMail.text.toString()
    }

    override fun getPhone(): String {
        return binding.edtPhone.text.toString()
    }

    override fun printMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showErrorUsernameMessage(message: String) {
        binding.tlUsername.error = message
    }

    override fun hideUsernameErrorMessage() {
        binding.tlUsername.error = null
    }

    override fun showErrorPasswordMessage(message: String) {
        binding.tlPassword.error = message
    }

    override fun hidePasswordErrorMessage() {
        binding.tlPassword.error = null
    }

    override fun showErrorNameMessage(message: String) {
        binding.tlName.error = message
    }

    override fun hideNameErrorMessage() {
        binding.tlName.error = null
    }

    override fun showErrorLastNameMessage(message: String) {
        binding.tlLastName.error = message
    }

    override fun hideLastNameErrorMessage() {
        binding.tlLastName.error = null
    }

    override fun showErrorAddressMessage(message: String) {
        binding.tlAddress.error = message
    }

    override fun hideAddressErrorMessage() {
        binding.tlAddress.error = null
    }

    override fun showErrorMailMessage(message: String) {
        binding.tlMail.error = message
    }

    override fun hideMailErrorMessage() {
        binding.tlMail.error = null
    }

    override fun showErrorPhoneMessage(message: String) {
        binding.tlPhone.error = message
    }

    override fun hidePhoneErrorMessage() {
        binding.tlPhone.error = null
    }

    override fun signUpSuccess(title: String, message: String) {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle(title)
        dialog.setMessage(message)
        dialog.setPositiveButton(getString(R.string.str_ok)) { _: DialogInterface, _: Int ->
            presenter.goToHome()
        }
        dialog.show()
    }

    override fun goToHome() {
        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    override fun getContext(): Context = this
}