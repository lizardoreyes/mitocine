package com.lizardoreyes.mitocine

import android.content.Context

interface BaseView {
    fun getContext(): Context
//    fun showLoading(message: String)
//    fun hideLoading()
}