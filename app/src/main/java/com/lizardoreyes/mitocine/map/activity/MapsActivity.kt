package com.lizardoreyes.mitocine.map.activity

import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.model.Place
import com.lizardoreyes.mitocine.R
import com.lizardoreyes.mitocine.databinding.ActivityMapsBinding

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        binding.btnSearch.setOnClickListener {
            val address = binding.edtSearch.text.toString()
            // Geocoder - Nos permite ubicar direcciones
            val geocoder = Geocoder(this)
            val results = geocoder.getFromLocationName(address, 1)

            if(results.isNotEmpty()) {
                val addressResult = results[0]
                val latLng = LatLng(
                    addressResult.latitude,
                    addressResult.longitude
                )

                mMap.addMarker(MarkerOptions().position(latLng).title("Huaral"))
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f))
            } else {
                Toast.makeText(this, "No se encontraron ubicaciones.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val huaral = LatLng(-11.495448919279893, -77.20708854969936)
        val zoomLevel = 15.0f //This goes up to 21
        mMap.addMarker(MarkerOptions().position(huaral).title("Huaral"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(huaral, zoomLevel))

        if (intent.hasExtra("place")) {
            val place = intent.getParcelableExtra<Place>("place")
            if (place != null) {
                mMap.addMarker(MarkerOptions().position(place.latLng).title(place.address))
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.latLng, 15f))
            }
        }
    }
}