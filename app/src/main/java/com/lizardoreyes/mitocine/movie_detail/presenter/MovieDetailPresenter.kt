package com.lizardoreyes.mitocine.movie_detail.presenter

import com.lizardoreyes.mitocine.BasePresenter
import com.lizardoreyes.mitocine.movie_detail.views.MovieDetailView

class MovieDetailPresenter: BasePresenter<MovieDetailView> {
    lateinit var view: MovieDetailView

    override fun init(view: MovieDetailView) {
        this.view = view
    }
}