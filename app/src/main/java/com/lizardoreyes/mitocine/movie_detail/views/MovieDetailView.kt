package com.lizardoreyes.mitocine.movie_detail.views

import android.content.Context

interface MovieDetailView {

    fun context(): Context
}