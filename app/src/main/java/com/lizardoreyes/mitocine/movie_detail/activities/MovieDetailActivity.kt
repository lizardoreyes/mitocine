package com.lizardoreyes.mitocine.movie_detail.activities

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.lizardoreyes.mitocine.databinding.ActivityMovieDetailBinding
import com.lizardoreyes.mitocine.home.model.Movie
import com.lizardoreyes.mitocine.movie_detail.presenter.MovieDetailPresenter
import com.lizardoreyes.mitocine.movie_detail.views.MovieDetailView

class MovieDetailActivity : AppCompatActivity(), MovieDetailView {
    lateinit var binding: ActivityMovieDetailBinding
    lateinit var presenter: MovieDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMovieDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter = MovieDetailPresenter()
        presenter.init(this)

        if (intent.hasExtra("movie")) {
            val movie = intent.getParcelableExtra<Movie>("movie")
            binding.tvTitle.text = movie?.title
            Glide.with(this).load(movie?.imageUrl).into(binding.ivLogo)
        }
    }

    override fun context(): Context {
        return this
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}