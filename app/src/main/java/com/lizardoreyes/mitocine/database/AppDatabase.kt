package com.lizardoreyes.mitocine.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

// La versión es importante porque cuando publicas una versión
// esta sera reemplazada solo si aumentas la version de la base de datos
@Database(entities = [Person::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun personDao(): PersonDao

    companion object {
        private var instance: AppDatabase? = null

        @Synchronized
        fun getInstance(context: Context): AppDatabase {
            if (instance == null) {
                // Nombre de la base de datos
                instance = Room.databaseBuilder(context, AppDatabase::class.java, "mitocine").build()
            }
            return instance as AppDatabase
        }
    }
}